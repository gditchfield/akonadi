/*
    SPDX-FileCopyrightText: 2019 Daniel Vrátil <dvratil@kde.org>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef AKSTD_H_
#define AKSTD_H_

#include <functional>

#include <QHash>
#include <QString>

/// A glue between Qt and the standard library

namespace std
{
}

#endif
